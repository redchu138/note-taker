import React from 'react';
import { IconButton } from '@material-ui/core';
import {
  AddAlertOutlined,
  PersonAddOutlined,
  PaletteOutlined,
  ImageOutlined,
  ArchiveOutlined,
  Undo,
  Redo
} from '@material-ui/icons';
import DropDown from './DropDown.js';

function EditNoteToolBar ({
    onDelete
}) {

    return(
        <React.Fragment>
            <IconButton><AddAlertOutlined fontSize="small" /></IconButton>
            <IconButton><PersonAddOutlined fontSize="small" /></IconButton>
            <IconButton><PaletteOutlined fontSize="small" /></IconButton>
            <IconButton><ImageOutlined fontSize="small" /></IconButton>
            <IconButton><ArchiveOutlined fontSize="small" /></IconButton>
            <DropDown onDelete={onDelete}/>
            <IconButton><Undo fontSize="small" /></IconButton>
            <IconButton><Redo fontSize="small" /></IconButton>
        </React.Fragment>
    );

}

export default EditNoteToolBar;