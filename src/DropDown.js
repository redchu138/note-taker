import React from 'react';
import {
    Popper,
    IconButton,
    MenuItem,
    MenuList,
    Paper,
    ClickAwayListener
} from '@material-ui/core';
import { MoreVert } from '@material-ui/icons';

function DropDown({
    onDelete,
    onAddLabel
}) {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [showAddLabel, setShowAddLabel] = React.useState(false);
    const [labelInput, setLabelInput] = React.useState('');

    const handleClick = (event) => {
        setAnchorEl(anchorEl ? null : event.currentTarget);
    }

    const toggleShowAddLabel = () => {setShowAddLabel(!showAddLabel); console.log(showAddLabel)}

    function popperContents(showAddLabel) {
        if (showAddLabel) {
            return(
                <ClickAwayListener
                    onClickAway={() => {
                        handleClick(); 
                        toggleShowAddLabel();
                    }}
                >
                    <Paper elevation={4}>
                        <div>
                            <input onChange={event => setLabelInput(event.target.value)}></input>
                            <button onClick={() => {
                            }}>
                                Add
                            </button>
                        </div>
                    </Paper>
                </ClickAwayListener>
            )
        } else {
            return(
                <Paper elevation={4}>
                    <ClickAwayListener onClickAway={handleClick}>
                        <MenuList>
                            <MenuItem 
                                onClick={() => {
                                    handleClick();
                                    onDelete();
                                }}
                            >
                                Delete note
                            </MenuItem>
                            <MenuItem 
                                onClick={() => {
                                    toggleShowAddLabel();
                                    //onAddLabel();
                                }}
                            >
                                Add labels
                            </MenuItem>
                            <MenuItem onClick={handleClick}>Add drawing</MenuItem>
                            <MenuItem onClick={handleClick}>Make a copy</MenuItem>
                            <MenuItem onClick={handleClick}>Make checkboxes</MenuItem>
                            <MenuItem onClick={handleClick}>Copy to Google Docs</MenuItem>
                        </MenuList>
                    </ClickAwayListener>
                </Paper>
            )
        }
    }

    return (
        <div>
            <IconButton onClick={ handleClick }>
                <MoreVert fontSize='small' />
            </IconButton>
            <Popper 
                placement='bottom-start' 
                open={Boolean(anchorEl)} 
                anchorEl={anchorEl}
            >
                {popperContents(showAddLabel)}
            </Popper>
        </div>
    );
}

export default DropDown;