import React, { useState } from 'react';
import {
  Card,
  CardActions,
  Typography
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useFocus, useMountEffect } from './hooks';
import ContentEditable from 'react-contenteditable';
import classnames from 'classnames';
import { CARD_COLORS } from './Constants'
import './prototype.css';

const useStyles = makeStyles({
  card: {
    maxWidth: '600px',
    borderRadius: '8px',
    paddingTop: '12px',
    flex: '1 1 auto',
    width: '600px'
  },
  title: {
    width: '100%',
    padding: '0 15px 4px 15px',
    fontSize: '16px',
    '&:focus': {
      border: 'none',
      outline: 'none'
    }
  },
  text: {
    width: '100%',
    padding: '0 16px 12px 16px',
    fontSize: '14px',
    letterSpacing: '.015em',
    '&:focus': {
      border: 'none',
      outline: 'none'
    }
  },
  cardActions: {
    padding: 0,
    marginLeft: '4px',
    height: '42px'
  },
  actionButton: {
    marginLeft: 'auto',
    marginRight: '15px',
    textTransform: 'none'
  },
  extraSpace: {
    paddingBottom: '22px'
  }
});

function Note({
  editable,
  onChange,
  note = {},
  raised,
  actions,
  showPlaceholder,
  hover,
  extraSpace
}) {
  const classes = useStyles();
  const [inputRef, setInputRefFocus] = useFocus();
  const [isHovered, setIsHovered] = useState(false);

  useMountEffect(setInputRefFocus);

  const color = note.color || CARD_COLORS.Default;

  return (
    <Card
      style={{ backgroundColor: color.background, borderColor: color.border }}
      className={classes.card}
      variant={raised ? 'elevation' : 'outlined'}
      elevation={5}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <Typography fontWeight="fontWeightBold" component="div" >
        {
          (note.title || showPlaceholder) && (
            <ContentEditable
              className={classnames(
                classes.title,
                'text-box-title',
                { [classes.extraSpace]: extraSpace }
              )}
              html={note.title}
              disabled={!editable}
              onChange={e => onChange({ ...note, title: e.target.value })}
              placeholder={showPlaceholder && 'Title'}
            />
          )
        }
      <ContentEditable
        className={`${classes.text} text-box-text`}
        html={note.text}
        disabled={!editable}
        onChange={e => onChange({ ...note, text: e.target.value })}
        placeholder={showPlaceholder && "Take a note..."}
        innerRef={inputRef}
      />
      </Typography>
      <CardActions disableSpacing className={classes.cardActions} >
        {
          (!(hover && !isHovered)) && actions
        }
      </CardActions>
    </Card>
  );
}

export default Note;
