import { useEffect, useRef } from 'react';

export function useClickOutsideHandler(ref, onClickOutside) {
  const handler = (e) => {
    if (ref.current && !ref.current.contains(e.target)) {
      onClickOutside(e);
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handler);
    return () => document.removeEventListener('mousedown', handler);
  });
}

export function useFocus() {
  const elRef = useRef(null);
  const setFocus = () => (elRef.current && elRef.current.focus());
  return [elRef, setFocus];
}

export function useMountEffect(func) {
  return useEffect(func, []);
}
