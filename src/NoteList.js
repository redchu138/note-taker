import React from 'react';
import Note from './Note.js'
import './note-taker.css';
import EditNoteToolBar from './EditNoteToolBar.js';

function NoteList({
  onDelete,
  onAddLabel,
  notes
}) {
  return (
    <div className="notes-container">
      {
        notes.map(note => (
          <Note 
            actions={<EditNoteToolBar 
                      onDelete={() => onDelete(note.key)} 
                    />} 
            note={note}
            hover
          />
        ))
      }
    </div>
  );
}

export default NoteList;
