import React, { useState } from 'react';
import {
  Popper,
  IconButton,
  Paper
} from '@material-ui/core';
import { PaletteOutlined , Check } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { CARD_COLORS } from './Constants';

const useStyles = makeStyles({
  button: {
    border: '2px solid',
    '&:hover': {
      borderColor: '#000 !important'
    },
    margin: '4px 2px',
    width: '28px',
    height: '28px'
  },
  paper: {
    padding: '4px 6px',
    width: '128px'
  }
});

export default ({
  onSelect,
  selected,
  onOpen
}) => {
  const [anchor, setAnchor] = useState(null);
  const toggleDropdown = e => {
    setAnchor(anchor ? null : e.currentTarget);
    onOpen(!anchor);
  };
  const classes = useStyles();

  return (
    <div
      onMouseEnter={toggleDropdown}
      onMouseLeave={toggleDropdown}
    >
      <IconButton
      >
        <PaletteOutlined fontSize="small" />
      </IconButton>
      <Popper
        open={!!anchor}
        anchorEl={anchor}
        placement="top-start"
      >
        <Paper elevation={4} className={classes.paper} square >
          {
            Object.keys(CARD_COLORS).map(color => (
              <IconButton
                className={classes.button}
                style={{
                  backgroundColor: CARD_COLORS[color].background,
                  borderColor: CARD_COLORS[color].border
                }}
                onClick={e => onSelect(CARD_COLORS[color])}
              >
                { (color === selected) && <Check /> }
              </IconButton>
            ))
          }
        </Paper>
      </Popper>
    </div>
  );
};
