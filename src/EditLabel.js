import React, { useState, useRef } from 'react';
import {
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  Typography,
  IconButton
} from '@material-ui/core';
import {
  Done,
  Label,
  Edit,
  Add,
  Close,
  Delete,
  Check
} from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  labelItemWrapper: {
    display: 'flex'
  },
  labelItemText: {
    flexGrow: 1
  },
  dialogContent: {
    padding: '15px',
    '&:first-child': {
      padding: '15px'
    }
  },
  dialog: {
    width: '300px',
    borderRadius: 'unset'
  },
  title: {
    fontSize: '1rem'
  },
  form: {
    color: 'grey',
    width: '270px',
    height: '45px',
    display: 'flex',
    alignItems: 'center'
  },
  formInput: {
    border: 'none',
    boxShadow: 'none',
    outline: 'none',
    padding: 0,
    fontSize: '14px',
    '&:focus': {
      borderBottom: '1px solid rgba(0, 0, 0, 0.2)'
    },
    fontWeight: 500,
    margin: '0 15px',
    height: '25px',
    flex: '1 1 auto',
    minWidth: 0
  }
}));

function Form({
  value,
  onChange,
  onSubmit
}) {
  const classes = useStyles();
  const [isActive, setIsActive] = useState(false);

  return (
    <div className={classes.form}>
      {
        !isActive ? (
          <IconButton onClick={() => setIsActive(true)} disableRipple >
            <Add fontSize="small" />
          </IconButton>
        ) : (
          <IconButton
            onClick={() => {
              setIsActive(false);
              onChange('');
            }}
            disableRipple
          >
            <Close fontSize="small" />
          </IconButton>
        )
      }

      <input
        className={classes.formInput}
        placeholder="Create new label"
        value={value}
        onChange={e => onChange(e.target.value)}
        onKeyPress={e => {
          if (e.which === 13) {
            onSubmit(value);
          }
        }}
        onFocus={() => setIsActive(true)}
      />

      {
        isActive && (
          <IconButton onClick={() => onSubmit(value)} disableRipple >
            <Done fontSize="small" />
          </IconButton>
        )
      }
    </div>
  );
}

function LabelItem({
  label,
  onUpdate,
  onDelete
}) {
  const classes = useStyles();
  const [isActive, setIsActive] = useState(false);
  const [cached, setCached] = useState(label);
  const [isHovered, setIsHovered] = useState(false);
  const inputRef = useRef();

  const startEditing = () => {
    inputRef.current.focus();
    setIsActive(true);
  }

  const endEditing = () => {
    onUpdate(cached);
    setIsActive(false);
    inputRef.current.blur();
  };

  return (
    <div
      className={classes.form}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      {
        (isActive || isHovered) ? (
          <IconButton onClick={() => onDelete(label)} disableRipple >
            <Delete fontSize="small" />
          </IconButton>
        ) : (
          <IconButton disableRipple >
            <Label fontSize="small" />
          </IconButton>
        )
      }

      <input
        ref={inputRef}
        className={classes.formInput}
        placeholder="Create new label"
        value={cached.text}
        onChange={e => setCached({ ...cached, text: e.target.value})}
        onKeyPress={e => {
          if (e.which === 13) {
            endEditing();
          }
        }}
        onFocus={startEditing}
        onBlur={endEditing}
      />

      {
        isActive ? (
          <IconButton onClick={endEditing} disableRipple >
            <Check fontSize="small" />
          </IconButton>
        ) : (
          <IconButton onClick={startEditing} disableRipple >
            <Edit fontSize="small" />
          </IconButton>
        )
      }

    </div>
  );
}

function EditLabel({
  open,
  setOpen,
  labels = [],
  setLabels
}) {
  const closeDialogue = () => setOpen(false);
  const classes = useStyles();
  const [currentValue, setCurrentValue] = useState('');

  const addLabel = (text) => {
    if (text) {
      setCurrentValue('');
      setLabels([
        { text, id: Date.now() },
        ...labels
      ]);
    }
  };

  const updateLabel = (updated) => {
    if (updated) {
      setLabels(labels.map(label => {
        if (label.id === updated.id) {
          return updated;
        }
        return label;
      }));
    }
  }

  const deleteLabel = (toDelete) => {
    if (toDelete) {
      const removed = labels.filter(label => label.id !== toDelete.id);
      setLabels(removed);
    }
  }

  return (
    <Dialog classes={{ paperWidthXs: classes.dialog }} open={open} maxWidth="xs" fullWidth >
      <DialogContent classes={{ root: classes.dialogContent }} dividers>
        <Typography className={classes.title} variant="subtitle2">Edit Labels</Typography>
        <Form
          value={currentValue}
          onChange={setCurrentValue}
          onSubmit={addLabel}
        />

        {
          labels.map(label => (
            <LabelItem
              key={label.id}
              autofocus
              label={label}
              onUpdate={updateLabel}
              onDelete={deleteLabel}
            />
          ))
        }

      </DialogContent>
      <DialogActions>
        <Button onClick={closeDialogue}>Done</Button>
      </DialogActions>
    </Dialog>
  );
}

export default EditLabel;
