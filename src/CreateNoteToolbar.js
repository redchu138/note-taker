import React from 'react';
import {
  IconButton,
  Button
} from '@material-ui/core';
import {
  AddAlertOutlined,
  PersonAddOutlined,
  ImageOutlined,
  ArchiveOutlined,
  Undo,
  Redo,
  MoreVert
} from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import ColorSelectDropdown from './ColorSelectDropdown';

const useStyles = makeStyles({
  actionButton: {
    marginLeft: 'auto',
    marginRight: '15px',
    textTransform: 'none'
  }
});

function CreateNoteToolbar ({
  onClose,
  color,
  onColorChange,
  onMenuOpen
}) {
  const classes = useStyles();
  return (
    <React.Fragment>
      <IconButton><AddAlertOutlined fontSize="small" /></IconButton>
      <IconButton><PersonAddOutlined fontSize="small" /></IconButton>
      <ColorSelectDropdown selected={color} onSelect={onColorChange} onOpen={onMenuOpen} />
      <IconButton><ImageOutlined fontSize="small" /></IconButton>
      <IconButton><ArchiveOutlined fontSize="small" /></IconButton>
      <IconButton><MoreVert fontSize="small" /></IconButton>
      <IconButton><Undo fontSize="small" /></IconButton>
      <IconButton><Redo fontSize="small" /></IconButton>
      <div className={classes.actionButton}>
        <Button
          style={{ textTransform: 'none' }}
          onClick={onClose}
        >
          Close
        </Button>
      </div>
    </React.Fragment>
  );
}

export default CreateNoteToolbar;
