import React, { Component } from "react";
import NoteList from "./NoteList.js"
import NewNoteForm from './NewNoteForm';
import { CARD_COLORS } from './Constants';

import './note-taker.css';

const TEST_NOTES = [
  {
    title: 'title only',
    text: '',
    key: 5,
    labels: ['dog', 'cat'],
    color: CARD_COLORS.Default
  },
  {
    title: 'two',
    text: 'one one one',
    key: 2,
    labels: ['cat'],
    color: CARD_COLORS.Gray
  }
];

class NoteTaker extends Component {
    constructor(props) {
        super(props);

        this.state = {
            notes: TEST_NOTES
        }

        this.handleSubmitNote = this.handleSubmitNote.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.handleAddLabel = this.handleAddLabel.bind(this);
    }

    handleDelete(key) {
        var filteredNotes = this.state.notes.filter(note => (note.key !== key));
        this.setState({ notes: filteredNotes} );
    }

    handleAddLabel(key, label) {
        var changedNotes = this.states.notes;

        changedNotes.forEach((note) => {
            if (note.key == key) {
                note.labels.add(label);
                console.log('Label Added: ' + label);
            }
        });

        this.setState({changedNotes});
    }

    handleSubmitNote(note) {
        this.setState(state => ({
            notes: [note, ...state.notes]
        }));
    }

    handleEditNote(key, change) {
      this.setState(state => ({
        notes: state.notes.map(note => {
          if (note.key === key) {
            return change;
          }
          return note;
        })
      }));
    }

    render() {
        return (
            <div className="container">
                <NewNoteForm onSubmit={this.handleSubmitNote} />
                <NoteList 
                    notes={this.state.notes} 
                    onDelete={this.handleDelete}
                    onAddLabel={this.handleAddLabel}
                />
            </div>
        )
    }
}

export default NoteTaker;
