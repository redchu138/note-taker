export const CARD_COLORS = {
  Default: {
    border: '#e0e0e0',
    background: '#fff',
  },
  Red: {
    border: '#f28b82',
    background: '#f28b82',
  },
  Orange: {
    border: '#fbbc04',
    background: '#fbbc04',
  },
  Yellow: {
    border: '#fff475',
    background: '#fff475',
  },
  Green: {
    border: '#ccff90',
    background: '#ccff90'
  },
  Teal: {
    border: '#a7ffeb',
    background: '#a7ffeb',
  },
  Blue: {
    border: '#cbf0f8',
    background: '#cbf0f8',
  },
  'Dark blue': {
    border: '#aecbfa',
    background: '#aecbfa',
  },
  Purple: {
    border: '#d7aefb',
    background: '#d7aefb',
  },
  Pink: {
    border: '#fdcfe8',
    background: '#fdcfe8',
  },
  Brown: {
    border: '#e6c9a8',
    background: '#e6c9a8',
  },
  Gray: {
    border: '#e8eaed',
    background: '#e8eaed',
  }
};
