import React from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import { HashRouter } from 'react-router-dom';

import "./index.css";
import App from './App';
import { grey } from "@material-ui/core/colors";

const theme = createMuiTheme({
  palette: {
    primary: grey
  },
  typography: {
    htmlFontSize: 18
  }
});

const container = document.querySelector("#container");
ReactDOM.render(
  <ThemeProvider theme={theme} >
    <HashRouter hashType="noslash" ><App /></HashRouter>
  </ThemeProvider>,
  container
);
