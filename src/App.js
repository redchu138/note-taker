import React, { useState } from 'react';
import {
  AppBar,
  Toolbar,
  IconButton,
  InputBase,
  Typography,
  Drawer,
  List,
  ListItem,
  ListItemText,
  Divider,
  ListSubheader
} from '@material-ui/core';
import {
  Menu,
  Search,
  EmojiObjectsOutlined,
  NotificationsOutlined,
  EditOutlined,
  ArchiveOutlined,
  DeleteOutline,
  LabelOutlined
} from '@material-ui/icons';
import { fade, makeStyles } from '@material-ui/core/styles';
import classnames from 'classnames';
import { Link, useLocation } from 'react-router-dom';
import { upperFirst } from 'lodash'

import NoteTaker from './NoteTaker';
import EditLabel from './EditLabel';

const drawerWidth = 280;
const useStyles = makeStyles(theme => ({
  root: {
    height: '100%',
    width: '100%'
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
    display: 'flex',
    alignItems: 'center'
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
    fontWeight: 300,
    width: '160px',
    marginLeft: '10px'
  },
  toolbar: theme.mixins.toolbar,
  appbar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: '#fff',
    boxShadow: 'none',
    borderBottom: '1px solid rgba(0, 0, 0, .12)'
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    position: 'fixed',
    borderRight: 'none'
  },
  drawerPaper: {
    width: drawerWidth,
    borderRight: 'none'
  },
  content: {

  },
  contentWrapper: {
    height: '100%',
    width: '100%'
  },
  withDrawerOffset: {
    padding: `0 0 0 ${drawerWidth}px`,
    width: `calc(100% - ${drawerWidth}px)`
  },
  drawerItemText: {
    paddingLeft: '32px'
  },
  logo: {
    height: '100%',
    textAlign: 'center',
    marginRight: '4px'
  },
  brand: {
    display: 'flex',
    justifyItems: 'center'
  },
  logoSvg: {
    verticalAlign: 'text-bottom'
  },
  listItem: {
    background: 'inherit',
    padding: '4px 0 4px 24px',
    height: '48px',
    borderTopRightRadius: '25px',
    borderBottomRightRadius: '25px',
    fontSize: '24px'
  },
  listItemText: {
    fontSize: '14px',
    fontWeight: 500,
    color: 'rgba(0, 0, 0, .87)'
  },
  link: {
    color: 'inherit',
    textDecoration: 'none',
    display: 'flex',
    alignItems: 'center',
    height: '100%',
    width: '100%'
  },
  yellow: {
    backgroundColor: '#feefc3'
  },
  grayOnHover: {
    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, .12)'
    },
  },
  subheader: {
    paddingLeft: '24px'
  }
}));

const DrawerItem = ({ icon, onClick, text, to }) => {
  const classes = useStyles();
  const { pathname } = useLocation();

  return (
    <ListItem
      className={classnames(
        { [classes.yellow]: (pathname === to) },
        { [classes.grayOnHover]: (pathname !== to) },
        classes.listItem
      )}
      onClick={onClick}
    >
      <Link className={classes.link} to={to || ''}>
        { icon }
        <ListItemText
          className={classes.drawerItemText}
          primary={<Typography className={classes.listItemText}>{text}</Typography>}
        />
      </Link>
    </ListItem>
  )
};

function Title() {
  const { pathname } = useLocation();
  const classes = useStyles();
  console.log(pathname);

  if (pathname === '/' || pathname === '/home') {
    return (
      <React.Fragment>
        <div className={classes.logo}><EmojiObjectsOutlined className={classes.logoSvg} /></div>
        <Typography variant="h5" >
          Keep
        </Typography>
      </React.Fragment>
    )
  }

  let formattedPath;
  if (pathname.includes('/label')) {
    formattedPath = pathname.replace(/\/label/, '');
    formattedPath = formattedPath.slice(1);
  } else {
    formattedPath = pathname;
    formattedPath = formattedPath.slice(1);
    formattedPath = upperFirst(formattedPath);
  }

  return formattedPath;
}

function App() {
  const classes = useStyles();
  const [isDrawerOpen, setIsDrawerOpen] = useState(true);
  const [isLabelEditOpen, setIsLabelEditOpen] = useState(false);
  const [labels, setLabels] = useState([
    { text: 'label 1', id: 1 },
    { text: 'label 2', id: 2 }
  ]);
  const openLabelEdit = () => setIsLabelEditOpen(true);

  return (
    <div className={classes.root}>
      <AppBar position="fixed" className={classes.appbar} disableGutters>
        <Toolbar>
          <IconButton fontSize="large" edge="start" onClick={() => setIsDrawerOpen(!isDrawerOpen)} >
            <Menu />
          </IconButton>

          <Typography className={classes.title} variant="h6" noWrap>
            <div className={classes.brand}>
              <Title />
            </div>
          </Typography>

          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <Search />
            </div>
            <InputBase placeholder="Search" />
          </div>

        </Toolbar>
      </AppBar>

      {
        isDrawerOpen && (
          <Drawer
            className={classes.drawer}
            variant="permanent"
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            <div className={classes.toolbar} />
            <List>
              <DrawerItem to="/home" icon={<EmojiObjectsOutlined color="action" fontSize="inherit" />} text="Notes" />
              <DrawerItem to="/reminders" icon={<NotificationsOutlined color="action" fontSize="inherit" />} text="Reminders" />
            </List>
            <Divider />
            <List subheader={<ListSubheader className={classes.subheader}>LABELS</ListSubheader>}>
              { labels.map(label => <DrawerItem key={label.id} to={`/label/${label.text}`} icon={<LabelOutlined color="action" fontSize="inherit" />} text={label.text} />) }
              <DrawerItem icon={<EditOutlined color="action" fontSize="inherit" />} text="Edit Labels" onClick={openLabelEdit} />
            </List>
            <Divider />
            <List>
              <DrawerItem to="/archive" icon={<ArchiveOutlined color="action" fontSize="inherit" />} text="Archived" />
              <DrawerItem to="/trash" icon={<DeleteOutline color="action" fontSize="inherit" />} text="Trash" />
            </List>
          </Drawer>
        )
      }

      <div className={classnames({ [classes.withDrawerOffset]: isDrawerOpen }, classes.contentWrapper)}>
        <div className={classes.toolbar} />
        <div className={classes.content}>
          <div>
            <NoteTaker />
          </div>
        </div>
      </div>

      <EditLabel
        open={isLabelEditOpen}
        setOpen={setIsLabelEditOpen}
        labels={labels}
        setLabels={setLabels}
      />
    </div>
  );
}

export default App;
