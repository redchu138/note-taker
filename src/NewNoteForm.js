import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Card,
  Button,
  Typography,
  IconButton,
  ClickAwayListener
} from '@material-ui/core';
import {
  CheckBoxOutlined,
  BrushOutlined,
  ImageOutlined
} from '@material-ui/icons';

import Note from './Note';
import ContentEditable from 'react-contenteditable';
import CreateNoteToolbar from './CreateNoteToolbar';
import './prototype.css';

const useStyles = makeStyles({
  container: {
    margin: '32px 0 28px',
  },
  card: {
    width: '600px',
    borderRadius: '8px',
    height: '46px'
  },
  typography: {
    fontSize: '16px',
    padding: '12px 16px',
    lineHeight: '22px',
    color: 'rgba(0, 0, 0, .38)',
    fontWeight: 500,
    display: 'flex',
    alignItems: 'center'
  },
  input: {
    '&:focus': {
      border: 'none',
      outline: 'none'
    },
    display: 'inline',
    flex: '1 1 auto'
  },
  button: {
    padding: 0,
    width: '48px'
  }
});

const INITIAL_STATE = {
  title: '',
  text: '',
  color: undefined,
  labels: new Set()
}

function NewNoteForm({
  onSubmit,
}) {
  const [isActive, setIsActive] = useState(false);
  const [note, setNote] = useState(INITIAL_STATE);
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const classes = useStyles();

  const endEditing = () => {
    if (note.title || note.text) {
      onSubmit({ ...note, key: Date.now() });
    }
    setNote(INITIAL_STATE);
    setIsActive(false);
  }

  if (!isActive) {
    return (
      <div className={classes.container}>
        <Card className={classes.card} elevation={5}>
          <Typography className={classes.typography} component="div">
            <ContentEditable
              className={`${classes.input} text-box-text`}
              placeholder="Take a note..."
              onClick={() => setIsActive(true)}
              html=""
            />
            <IconButton className={classes.button} >
              <CheckBoxOutlined />
            </IconButton>
            <IconButton className={classes.button} >
              <BrushOutlined />
            </IconButton>
            <IconButton className={classes.button} >
              <ImageOutlined />
            </IconButton>
          </Typography>
        </Card>
      </div>
    );
  }

  const onColorChange = color => setNote({ ...note, color });

  return (
    <ClickAwayListener onClickAway={() => {
      if (!isMenuOpen) {
        endEditing();
      }
    }} >
      <div className={classes.container}>
        <Note
          editable
          dismissButton={(
            <Button
              style={{ textTransform: 'none' }}
              onClick={endEditing}
            >
              Close
            </Button>
          )}
          note={note}
          onChange={setNote}
          raised
          focus
          showPlaceholder
          extraSpace
          actions={(
            <CreateNoteToolbar
              onClose={endEditing}
              onColorChange={onColorChange}
              color={note.color}
              onMenuOpen={setIsMenuOpen}
            />
          )}
        />
      </div>
    </ClickAwayListener>
  );
}

export default NewNoteForm;
